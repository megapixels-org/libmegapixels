#include <libmegapixels.h>

char *
libmegapixels_flash_get_led_path(const char *path);

void
libmegapixels_flash_cleanup_flash_path(libmegapixels_camera *camera);
