Frequently Asked Questions
==========================

What is libmegapixels?
   This is a library for accessing V4L2 cameras on modern ARM platforms that
   use the media-request pipeline to do routing of cameras to the SoC hardware
   to process the pixels in various ways. It's intended to be called instead of
   opening the plain V4L2 device so it can configure the hardware to a specific
   mode and then it hands over the V4L2 handles to let your application do the
   normal Linux camera procedures.

What is the correct way to display the name 'libmegapixels'?
   Just follow the correct grammar rules for your language. Being difficult
   about the capitalisation of project names is just a massive waste of time.

How is libmegapixels different from using V4L2
   Libmegapixels is just a glue layer on top of V4L2 to make writing
   applications that use the new media request pipeline a lot easier. Once the
   pipeline has been configured libmegapixels will just hand over a file
   descriptor for a regular V4L2 device to get the frames from.

Does this mean the camera stack is completely open?
   Yes, libmegapixels does not support the Android mess that is closed-source
   userspace sensor drivers and also provides an open source AAA algorithm.

   Using closed modules in libmegapixels is prohibited, but it's only a
   library for accessing V4L2 so whatever happens in the applications is up to
   the applications.