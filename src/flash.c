#include <libmegapixels.h>
#include <unistd.h>
#include <stdlib.h>
#include <limits.h>
#include "flash.h"
#include "util.h"

// Flash path is used for both the V4L path, and the LED path, and can be overwritten by a later stage, so memory is first freed
void
libmegapixels_flash_cleanup_flash_path(libmegapixels_camera *camera)
{
	if (camera->flash_path != NULL) {
		free(camera->flash_path);
		camera->flash_path = NULL;
	}
}

// Given the LED path (e.g., /sys/class/leds/white:flash), it'll append /flash_strobe at the end
char *
libmegapixels_flash_get_led_path(const char *path)
{
	char *result = malloc(PATH_MAX);
	if (result == NULL) {
		return NULL;
	}

	snprintf(result, PATH_MAX, "%s/flash_strobe", path);

	return result;
}

// Turns on the LED/V4L flash/strobe
void
libmegapixels_flash_on(libmegapixels_camera *camera)
{
	if (camera->flash_type == LIBMEGAPIXELS_FLASH_LED) {
		lseek(camera->flash_fd, 0, SEEK_SET);
		dprintf(camera->flash_fd, "1\n");
	} else if (camera->flash_type == LIBMEGAPIXELS_FLASH_V4L) {
		// Mode must be flash, and strobe source must be software, for strobe to be possible
		// Strobe source may not exist, which is fine, we ignore the result
		set_control(camera->flash_fd, V4L2_CID_FLASH_LED_MODE, V4L2_FLASH_LED_MODE_FLASH);
		set_control(camera->flash_fd, V4L2_CID_FLASH_STROBE_SOURCE, V4L2_FLASH_STROBE_SOURCE_SOFTWARE);

		// Starts the flash strobe. It will automatically stop
		set_control(camera->flash_fd, V4L2_CID_FLASH_STROBE, 1);
	}
}

// Turns off the LED/V4L flash/strobe, where applicable
void
libmegapixels_flash_off(libmegapixels_camera *camera)
{
	// Neither LED nor V4L currently needs any action in here, but in the future those or a different method may need to turn flash off here
	// For example, if flash strobe doesn't work, and we need to manually turn on/off torch mode
}
