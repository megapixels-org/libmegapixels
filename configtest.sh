#!/bin/bash

RED='\033[0;31m'
BOLDRED='\033[1;31m'
GREEN='\033[0;32m'
CLEAR='\033[0m'

ret=0
for cfg in $(find config/ -type f -iname *.conf)
do
  printf "%s: " $cfg
  errors=$(./build/megapixels-configlint "$cfg" 2>&1)
  if [ -z "$errors" ] ; then
    echo -e "${GREEN}PASS${CLEAR}"
  else
    ret=1
    echo -e "${BOLDRED}FAIL${CLEAR}"
    echo -e "${RED}$errors${CLEAR}"
    echo ""
  fi
done
exit $ret
