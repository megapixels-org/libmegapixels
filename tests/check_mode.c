#include <stdbool.h>
#include "greatest.h"
#include "libmegapixels.h"

static enum greatest_test_res
check_bytesused(
	uint32_t pixfmt,
	uint32_t width,
	uint32_t height,
	uint32_t expected_width_to_bytes,
	uint32_t expected_width_to_padding,
	uint32_t expected_bytes_used
)
{
	int format = libmegapixels_v4l_pixfmt_to_index(pixfmt);
	uint32_t width_to_bytes = libmegapixels_mode_width_to_bytes(format, width);
	uint32_t width_to_padding = libmegapixels_mode_width_to_padding(format, width);
	uint32_t bytes_used = (width_to_bytes + width_to_padding) * height;
	// printf(
	// 	"\nFormat name: %s, width: %u, height: %u, width to bytes: %u, width to padding: %u, bytes used: %u\n",
	// 	libmegapixels_format_name(format),
	// 	width,
	// 	height,
	// 	width_to_bytes,
	// 	width_to_padding,
	// 	bytes_used
	// );
	ASSERT_EQ(width_to_bytes, expected_width_to_bytes);
	ASSERT_EQ(width_to_padding, expected_width_to_padding);
	ASSERT_EQ(bytes_used, expected_bytes_used);
	PASS();
}

TEST bytesused_for_rggb8(void)
{
	CHECK_CALL(check_bytesused(V4L2_PIX_FMT_SRGGB8, 4208, 3120, 4208, 0, 13128960));

	PASS();
}

TEST bytesused_for_rggb10(void)
{
	CHECK_CALL(check_bytesused(V4L2_PIX_FMT_SRGGB10, 4208, 3120, 8416, 0, 26257920));

	PASS();
}

TEST bytesused_for_rggb10p(void)
{
	CHECK_CALL(check_bytesused(V4L2_PIX_FMT_SRGGB10P, 4208, 3120, 5260, 4, 16423680));

	PASS();
}

SUITE (test_suite)
{
		RUN_TEST(bytesused_for_rggb8);
		RUN_TEST(bytesused_for_rggb10);
		RUN_TEST(bytesused_for_rggb10p);
}

GREATEST_MAIN_DEFS();

int
main(int argc, char **argv)
{
	GREATEST_MAIN_BEGIN();
	RUN_SUITE(test_suite);
	GREATEST_MAIN_END();
}
