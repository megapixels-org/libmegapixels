#include <libmegapixels.h>
#include <stdio.h>
#include <stdbool.h>

int
main(int argc, char *argv[])
{
	if (argc != 2) {
		fprintf(stderr, "Usage: %s <configfile>\n", argv[0]);
		return 1;
	}

	libmegapixels_devconfig *config = {0};
	libmegapixels_init(&config);
	if (!libmegapixels_load_file_lint(config, argv[1], 1)) {
		return 1;
	}

	if (config->count == 0) {
		fprintf(stderr, "Config file does not define any cameras\n");
		return 1;
	}

	bool faulty = false;
	for (int i = 0; i < config->count; i++) {
		if (config->cameras[i]->num_modes == 0) {
			fprintf(stderr, "No modes are defined for sensor '%s'\n", config->cameras[i]->name);
			faulty = true;
		}
	}
	if (faulty) {
		return 1;
	}
	return 0;
}