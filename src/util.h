#pragma once

int
xioctl(int fd, int request, void *arg);

void
set_control(int fd, unsigned int ctrl_id, signed int ctrl_val);

char *
pixfmt_to_str(unsigned int pixfmt);